## 1. Introducción

![](https://i.imgur.com/WCf9itA.png)

Las especies pertenecientes al grupo de **Animalia** se caracterizan por su amplia capacidad de movimiento, por no tener cloroplasto (aunque hay excepciones, como en el caso de Elysia chlorotica) ni pared celular, y por su desarrollo embrionario; que atraviesa una fase de blástula y determina un plan corporal fijo (aunque muchas especies pueden sufrir una metamorfosis posterior como los artrópodos).

Los **Cordados** (Chordata, del griego χορδωτά khordota 'con cuerda') son un filo del reino animal caracterizado por la presencia de una cuerda dorsal o notocorda (o notocordio) de células turgentes,​ tubo neural hueco en posición dorsal, hendiduras branquiales y cola, por lo menos en alguna fase del desarrollo embrionario.

Los **Ichthyomammalia** son un grupo de mamíferos como los cetáceos pero que comparten rasgos comunes con los *condrictios* y por la presencia de buena parte de un esqueleto cartilaginoso, y los *osteictios*, que corresponde a los peces óseos. El enfoque en uno de sus infraclases es el del ovoviviparismo con un caso singulr de matrotrofia placentaria. En este grupo, el embrión dentro de los huevos bentónicos consumen pronto el suministro, y es la placenta la que se encrga de proporcionar el suministro adicional.

Los **Syreniformes** corresponden a todas las especies de sirenas y sirenos que presentan una parte superior *homínida* y una parte inferior similar a la aleta de un tiburón o cualquier otro pez. Se agrupan todas las variaciones, ya sean los syreniformes que poseen membranas en lugar de oídos más similares al de los humanos, los que poseen aspecto más humano, syreniformes que viven en aguas abisales, otros que poseen aletas ventrales, otros que no, merfolks, etc.

Los grupos debajo de este orden todos son muy similares en común pero lo diferencian el tipo de dieta común. Puramente crnívoros, o puramente hervívoros, o bien ciertos rasgos encefálicos que los hacen actuar más o menos agresivos. El caso de esta documentación se trata al **Homo Sirenis** un caso de sirenas y tritones mnivos, cuya agresividad es baja, y actúan y razonan de manera casi idéntica a los humanos.