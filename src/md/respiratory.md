## 1. Aparato respiratorio pulmo-branquial

El homo sirenis es una especie cuyo aparato respiratorio singular guarda similitud con los dipnoi. Sin embargo, a diferencia de estos peces pulmonados, el homo sirenis no llega a perder la capacidad de respirar por las branquias en la adultez, por lo que pueden respirar bajo el agua de manera indefinida sin tener que salir a la superficie a tomar aire por los pulmones.

## 2. La hematosis (La forma de obtener oxígeno)

Antes de explicar la forma en que alternan la respiración por branquias o por pulmones en el caso del homo sirenis, es preciso diferenciar las distintas formas de obtener oxígeno entre las distintas especies branquiales:

Por un lado estan **los osteicos** (peces comunes) que poseen opérculos que actuan como tapas para controlar el flujo del agua que entra por la boca y expulsan por las agallas. Por otro lado están los **tiburones** que presentan branquias desnudas (sin opérculos) lo que les obliga a estar en movimiento para poder hacer pasar el agua a través de ellas. Y finalmente están los **pulpos** que poseen un sifón para controlar la succión y expulsión del agua (y que además actúa como válvula) en una cavidad donde se encuentran las branquias 


## 3. Dentro del agua

En el caso del homo sirenis, al igual que los tiburones, poseen aberturas desnudas en la zona del cuello por las que se filtra el agua, sin embargo, las branquias no se encuentran pegadas, debajo de éstas, sino que dichas aberturas confluyen en una especie de sacos cilindricos y alargados que descienden paralelos al esófago, donde se hallan las branquias, y cuya entrada es gestionada por sifones que succionan y expulsan el agua como los pulpos.

Debido a la presencia de pulmones y branquias simultaneas, el cuerpo de los sirenos y sirenas se ha adaptado para evitar el encharcamiento de los primeros, aunque dicho órgano permanezca más tiempo inactivo que las bránquias, es por eso que los pulmones de los homo sirenis están menos desarrolldos que el de los humanos, y en lugar de poseer una epiglotis para evitar que se cuele el agua en los bronquios, disponen de una válvula más sofisticada que se cierra de manera involuntaria dependiendo de la presión hidrostática, es decir, la presión ejercida por el agua sobre el cuerpo, como si poseyeran un sensor interno que detectara el momento en que su cuerpo se expone a un aumento de presión durante el descenso al fondo marino.

## 4. Fuera del agua

Cuando el homo sirenis se encuetra fuera del agua, el homo sirenis dispone de un mecanismo interno que se encarga de segregar una sustancia hipertónica (sustancia oxigenada con solutos) en los sacos alargados donde se hallan las branquias permanecen hidratadas y por esta razón no se secan una vez se encuentra fuera del agua, pudiendo permanecer por bastante tiempo fuera de dicho medio sin que se estropeen. Como tienen que producir y secretar más sustancia hipertónica para renovarla, es importante tener en cuenta que la producción y secreción de esta sustancia requiere de energía y recursos del cuerpo, por lo que debe equilibrar esta necesidad con la necesidad de mantenerse hidratado y evitar la deshidratación.

Un dato curioso es que las hendiduras del cuello del sireno o sirena conectan tanto con la faringe como con los sacos branquiales, y además, los orificios nasales no conectan con los pulmones, sino que su función es meramente olfativa. De esta manera, los sirenos poseen también dos vías para inhalar el aire, así como los humanos, sólo que en su caso estas vías son la boca y las hendiduras del cuello.

## 5. Así evitan la descompresión del buzo

El homo sirenis utiliza el mismo mecanismo que otros mamíferos marinos para no sufrir la misma patología de descompresión que afecta a los buceadores que emergen demasiado rápido. Esto se debe a la presencia de dos regiones pulmonares diferentes sumado a la estructura de sus pechos y su caja torácica cartilaginosa que permite que sus pulmones se compriman mientras se encuentran bajo el agua como lo hacen los cetáceos, además, sumado a esto, poseen un mecanismo especial (esfínteres pulmonares) para evitar que la sangre riegue los pulmones durante la inmersión, al igual que las tortugas. De esta forma evitan solubilizar el nitrógeno que les provocaría el embolismo. 

Cuando el cuerpo se somete a presión, el poco aire que quedó en los pulmones es expulsado tras abandonar la superficie, dejando una región medio vacía, separada de otra que se encuentra colapsada, a compresión. La sangre fluye exclusivamente a través de la región colapsada, lo que causa lo que se denomina "una falta de coincidencia entre la ventilación y la perfusión", que permite que el oxígeno y el dióxido de carbono sean absorbidos por el torrente sanguíneo, a la vez que minimiza o evita el intercambio de nitrógeno. De esta manera se protegen de la ingesta de cantidades excesivas de nitrógeno y, por lo tanto, se minimiza el riesgo del trastorno de descomprensión.

A pesar de no tener problemas con el intercambio de gases en el desceneso a una profundidad de máximo mil metros, son incapaces de descender a profundidades abisales (seis veces mayor) debido a la fuerte presión que ejerce en sus cuerpos, comprimiendo sus cráneos que no están preparados para soportarlo. Además, de ser incapaces de sobrevivir con el escaso oxígeno de las profundidades y la dificultad para hallar comida.
