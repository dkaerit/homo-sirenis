⚠️ En construcción ⚠️

* author: [@imhalfish (twitter)](https://twitter.com/naminohire)
* rp-account: [@blxckfine (twitter)](hhttps://twitter.com/blxckfine)

Esta documentación está basada compeltamente en la ficción. Su finalidad es la de imaginar la anamotía de un sireno y su clasificación en el reino animal de modo que se detallen aquí todas las cracterísticas de su especie, atendiendo a rasgos reales de otros animales que fundamenten la realidad de los mismos.

Pretende ser una fuente que pueda ser recurrida por cualquier persona para tener una noción en la que basarse a la hora de narrar historias que traten con sirenos y sirenas, si quieren ser concretos con según que detalles y no dejarlo a explicaciones como que "es cosa de magia", motivo que tampoco sea tachable debido a que la misma especie que se va a analizar aquí es tan ficticia como la magia.

Agradecimientos a cierta persona (sí tú, sabes a quién me refiero) porque si nunca me hubiera llegado a preguntar, dando lugar a la siguiente reflexión: *"¿Por qué los sirenos que interpretas tenien ombligo y mamas si éstos nacen de huevos, y en teoría no deberían anatómicamente presentarlos?"*, no me habrían empezado a surgir varias dudas que luego querría plasmar, intentando darle una explicación lo más científica posible dentro de la ficción.