Los sirenos y sirenas de la especie homo sirenis no son capaces de engendrar un cruce estéril con cualquier otra especie, a pesar de las similutides físicas aparentes. 

Es la diferencia de su aparato reproductor y su gestación ovovivípara la que hace imposible dicho resultado. Sin embargo, sucede un caso extraño en que, si el esperma de un humanoide terrestre fecundara los huevos de un homo sirenis, en ellos se gestarían una especie específica de peces, que varía según la raza que los fecundó. En el caso de un humano, engendraría en ellos `peces rostratus`.

